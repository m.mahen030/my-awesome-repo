from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Mohamad Mahendra' # TODO Implement this

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age' : calculate_age(1997)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    today = date.today()
    mhs_age = today.year - birth_year
    return mhs_age